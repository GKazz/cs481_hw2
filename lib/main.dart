import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    Widget titleSection = Container(
      padding: const EdgeInsets.all(15),
      child: Row(
        children: [
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                  padding: const EdgeInsets.only(bottom: 4),
                  child: Text('Trakai Castle', style: TextStyle(fontSize: 25, fontWeight: FontWeight.bold,),
                  ),
                ),
                Text('One of the castles in my home country Lithuania!',
                  style: TextStyle(fontWeight: FontWeight.bold, fontStyle: FontStyle.italic, color: Colors.blueGrey),
                ),
              ],
            ),
          ),
          Icon(Icons.home, color: Colors.red[500], ),
          Text('26'),
        ],
      ),
    );
    Color color = Theme.of(context).primaryColor;
    Color color2 = Colors.green;
    Color color3 = Colors.purple;

    Widget buttonSection = Container(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          _buildButtonColumn(color2, Icons.filter_hdr, 'Camping'),
          _buildButtonColumn(color3, Icons.computer, 'Gaming'),
          _buildButtonColumn(color, Icons.ac_unit, 'Snowboarding'),
        ],
      ),
    );

    Widget textSection = Container(
      padding: const EdgeInsets.all(20),
      child: Text(
        '     I was born in Lithuania, which is a very small country in North Eastern Europe by the Baltic Sea.'
            ' My family moved to San Diego when I was three and I grew up here ever since.'
            ' Every few years I go back to Lithuania to visit family and discover more of my birth country.'
            ' In my free time I like to do a lot of exploring and camping.'
            ' During the winter I try to go snowboard as often as I can in Big Bear while we have some snow.'
            ' When I am not outside doing something I do like to play a lot of video games on my computer.'
            ' I enjoy to tinker with things whether thats my car or my computer, I like to see how things work and know how to fix them.'
            ' I am looking forward to learning more about mobile design in this class!',
        softWrap: true,
        style: TextStyle(fontSize: 14, color: Colors.black),),

    );
    return MaterialApp(
        title: 'Layout Homework',
        home: Scaffold(
           appBar: AppBar(
              title:Text('Layout Homework'),
           ),
           body: ListView(
                children: [
                  Image.asset(
                     'images/Gintas1.jpg',
                       width: 600,
                       height: 240,
                       fit: BoxFit.cover,
                  ),
                  titleSection,
                  buttonSection,
                  textSection
               ]
           ),
        ),
    );
  }

  Column _buildButtonColumn(Color color, IconData icon, String label) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Icon(icon, color: color),
        Container(
          margin: const EdgeInsets.only(top: 2),
          child: Text(
            label,
            style: TextStyle(
              fontSize: 18,
              fontWeight: FontWeight.w400,
              color: color,
            ),
          ),
        ),
      ],
    );
  }
}




